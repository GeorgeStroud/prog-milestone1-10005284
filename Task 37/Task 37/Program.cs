﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            var studyhours = 10;
            var contact = 5;

            Console.WriteLine("Please enter how many credits your course is");
            var credits = int.Parse(Console.ReadLine());
            Console.WriteLine("Please enter length of course in weeks");
            var weeks = int.Parse(Console.ReadLine());

            var weekstudy = (studyhours - contact);

            var total =(weekstudy * credits) / (weeks);

            Console.WriteLine($"in a {weeks} week period in a {credits} credit course you should study {total} hours per week ");

          

               
        }
    }
}
