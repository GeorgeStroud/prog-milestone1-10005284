﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assesment_1_question_5_10005284
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter 24 hour time, in the format 12:00, 13:00 ect");

            var time = Console.ReadLine();

            switch (time)
            {
                case "12:00":
                    Console.WriteLine("the time is 12 pm");
                    break;

                case "13:00":
                    Console.WriteLine("The time is 1pm");
                    break;

                case "14:00":
                    Console.WriteLine("The time is 2pm");
                    break;

                case "15:00":
                    Console.WriteLine("The time is 3pm");
                    break;

                case "16:00":
                    Console.WriteLine("The time is 4pm");
                    break;

                case "17:00":
                    Console.WriteLine("The time is 5pm");
                    break;

                case "18:00":
                    Console.WriteLine("The time is 6pm");
                    break;

                case "19:00":
                    Console.WriteLine("The time is 7pm");
                    break;

                case "20:00":
                    Console.WriteLine("The time is 8pm");
                    break;

                case "21:00":
                    Console.WriteLine("The time is 9pm");
                    break;

                case "22:00":
                    Console.WriteLine("The time is 10pm");
                    break;

                case "23:00":
                    Console.WriteLine("The time is 11pm");
                    break;

                case "24:00":
                    Console.WriteLine("The time is 12am");
                    break;
            
            }
        }
    }
}
