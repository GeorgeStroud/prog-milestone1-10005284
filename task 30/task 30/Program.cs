﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number");
            var a = int.Parse(Console.ReadLine());
            Console.WriteLine("Please enter a second number");
            var b = int.Parse(Console.ReadLine());

            var c =(a + b);
            Console.WriteLine($"{a} + {b} is {c}");
        }
    }
}
