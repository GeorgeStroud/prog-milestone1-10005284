﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assesment_1_question_33_10005284
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter number of students");
            var student = double.Parse(Console.ReadLine());

            var tute = Math.Round(student / 28, 2);

            Console.WriteLine($"If you have {student} students then you will require {tute} tutorial classes");
            Console.ReadLine();
        }
    }
}
