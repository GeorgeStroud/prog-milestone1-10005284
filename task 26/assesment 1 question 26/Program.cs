﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assesment_1_question_26
{
    class Program
    {
        static void Main(string[] args)
        {
            var color = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };
            Array.Sort(color);
            Console.WriteLine(string.Join(",", color));
        }

    }
}
