﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assesment_1_question_34_10005284
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter number of weeks, to convert to number of working days");
            var a = int.Parse(Console.ReadLine());
            
                var answer =  (a * 5);
            
            Console.WriteLine($"In {a} weeks there are {answer} working days");
        }
    }
}
