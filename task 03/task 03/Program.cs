﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Do you wish to convert km or miles?");

            var a = Console.ReadLine();

            if (a == "km")


            {
                Console.WriteLine("Please insert km to be converted into miles");
                var km = double.Parse(Console.ReadLine());

                var ml = 0.621371;

                var answer = Math.Round(km * ml, 2);

                Console.WriteLine($"The answer is {km} KM is {answer} miles");
            }
            else
            {
                Console.WriteLine("Please insert miles to be converted into km");
                var b = double.Parse(Console.ReadLine());

                var c = 1.609344;

                var answer2 = Math.Round(b * c, 2);

                Console.WriteLine($"The answer is {b} miles is {answer2} km");

            }
        }
    }
}
