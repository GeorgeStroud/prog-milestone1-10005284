﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29
{
    class Program
    {
        static void Main(string[] args)
        {
            var fox = "The quick brown fox jumped over the fence";
            foreach (var word in fox.TrimEnd('.').Split('.'))           
                Console.WriteLine(word.Trim().Split(' ').Count());
        }
    }
}
